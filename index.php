<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Atomic Project</title>
        <!-- Bootstrap -->
        <link href="Resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="Resource/css/style.css" rel="stylesheet">
        
    </head>
    <body>
       <div class="container">
        <h1>BITM - Web App Dev - PHP</h1>
     
        <br>
        <table class="table table-bordered table-condensed table-striped text-center">
            <thead>
                <tr class="success">
                    <th style="text-align: center;color:#D34A24;font-size: 18px">Sl.</th>
                    <th style="text-align: center;color:#D34A24;font-size: 18px">Project Name</th>
                </tr>
            </thead>
            <tbody> 
                <tr>
                    <td>01</td>
						<td><a href="views/BITM/SEIP106611/index.php">Address Book</a></td>
					</tr>
					
				
            </tbody>
        </table>
        
        <div id="sub_by">
            <h2>Submitted By</h2>
            <h3>The Backbenchers</h3>
            <p>SEIP ID : 106994,106510,109297,106611,106463,107225
			</p>
            <p>Batch : 11</p>
        </div>
      
		
	</div>
    </body>
</html>
